package main

import (
	"fmt"
	"strings"
)

type Thing = int

const (
	Tree Thing = iota
	Snow
)

func getInput() string {
	return `..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#`
}

func main() {
	treeCount := 0
	for r, l := range strings.Split(getInput(), "\n") {
		if string(l[r*3%len(l)]) == "#" {
			treeCount++
		}
	}
	fmt.Printf("treecount %v\n", treeCount)
}
